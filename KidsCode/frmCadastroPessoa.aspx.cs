﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KidsCode
{
    public partial class frmCadastroPessoa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void rbFuncionario_CheckedChanged(object sender, EventArgs e)
        {
            ocultaPainel();
            if (rbFuncionario.Checked)
            {
                panelFuncionario.Visible = true;
                rbAluno.Checked = false;
                limpaPanelAlunos();
            }
            else
            {
                panelAluno.Visible = true;
                rbFuncionario.Checked = false;
                limpaPanelFuncionario();
            }
        }

        protected void btSalvar_Click(object sender, EventArgs e)
        {

        }

        protected void btLimpar_Click(object sender, EventArgs e)
        {
            limpaCampos();
        }

        public void limpaCampos()
        {
            txtNome.Text = "";
            //txtDataNasc.Text = "";
            txtCpf.Text = "";
            txtTelefone.Text = "";
            txtEmail.Text = "";
            limpaPanelFuncionario();
            limpaPanelAlunos();
        }

        public void limpaPanelAlunos()
        {
            txtResponsavel.Text = "";
            txtTurma.Text = "";
            txtDataMatric.Text = "";
            txtDataTermino.Text = "";
            txtValorMensalidade.Text = "";
            txtNota1.Text = "";
            txtNota2.Text = "";
            txtMediaFinal.Text = "";
        }
        
        public void limpaPanelFuncionario()
        {
            rbFuncionario.Checked = true;
            rbAluno.Checked = false;
            ddlFuncao.SelectedIndex = 0;
            txtSalario.Text = "";
            txtDataAdmissao.Text = "";
            txtDataDemissao.Text = "";
            txtResponsavel.Text = "";
            txtTurma.Text = "";
            txtDataMatric.Text = "";
            txtDataTermino.Text = "";
            txtValorMensalidade.Text = "";
        }
      
        public void ocultaPainel()
        {
            panelFuncionario.Visible = false;
            panelAluno.Visible = false;
        }

    }
}

