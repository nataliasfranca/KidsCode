﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmCadastroPessoa.aspx.cs" Inherits="KidsCode.frmCadastroPessoa" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />

    <title>Kids Code</title>
    <link rel="shortcut icon" href="Imagens/code-icon-script.png" />

    <script type="text/javascript">
        function mascara_data(campo) { if (campo.value.length == 2) { campo.value += '/'; } if (campo.value.length == 5) { campo.value += '/'; } }
    </script>
</head>

<body>
    <form id="form1" runat="server">
        <div id="total">

            <%--Cabeçalho--%>
            <div id="topo" class="container-fluid">
                <div class="span12">
                    <div class="row span3 offset2">
                        <img id="logo-topo" src="../Imagens/Logo.png">
                    </div>
                </div>
            </div>

            <%--Conteudo--%>
            <div id="conteudo">
                <h1>Cadastro</h1>

                <asp:Panel runat="server" ID="panelDadosPessoais" class="panel panel-default" GroupingText="Dados Pessoais">
                    <asp:RadioButton ID="rbFuncionario" runat="server" Text="Funionário" GroupName="TipoFuncao" OnCheckedChanged="rbFuncionario_CheckedChanged" AutoPostBack="True" Checked="true" />
                    <asp:RadioButton ID="rbAluno" runat="server" Text="Aluno" GroupName="TipoFuncao" OnCheckedChanged="rbFuncionario_CheckedChanged" AutoPostBack="True" />
                    <br />
                    *Nome Completo<br />
                    <asp:TextBox ID="txtNome" runat="server" Width="380px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvNome" runat="server" ErrorMessage="Campo Obrigatório" ControlToValidate="txtNome" ForeColor="Red"></asp:RequiredFieldValidator><br />


                    *Data de Nascimento (dd/mm/aaaa)<br />
                    <asp:TextBox ID="txtDataNasc" runat="server" ValidationGroup="txtDataNasc" ControlToCompare="txtDataNasc" BorderStyle="Outset">*</asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Data Incorreta" ForeColor="Red" Type="Date" ControlToValidate="txtDataNasc" ></asp:CompareValidator>

                    <br />


                    Cpf<br />
                    <asp:TextBox ID="txtCpf" runat="server"></asp:TextBox>
                    <br />


                    E-mail<br />
                    <asp:TextBox ID="txtEmail" runat="server" Width="380px"></asp:TextBox>
                    <br />


                    Telefone<br />
                    <asp:TextBox ID="txtTelefone" runat="server"></asp:TextBox>
                </asp:Panel>

                <asp:Panel runat="server" ID="panelFuncionario" Visible="true" class="panel panel-default" GroupingText="Dados do Funcionário">
                    Função<br />
                    <asp:DropDownList ID="ddlFuncao" runat="server">
                        <asp:ListItem Value="P">Professor</asp:ListItem>
                        <asp:ListItem Value="D">Diretor</asp:ListItem>
                        <asp:ListItem Value="S">Secretária</asp:ListItem>
                        <asp:ListItem Value="L">Auxiliar de Limpeza</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    *Sálario<br />
                    <asp:TextBox ID="txtSalario" runat="server"></asp:TextBox>
                    <br />
                    Data de Adimissão (dd/mm/aaaa)<br />
                    <asp:TextBox ID="txtDataAdmissao" runat="server" MaxLength="10" onKeyUp="mascara_data(this)"></asp:TextBox>
                    <br />
                    Data de Demissão (dd/mm/aaaa)<br />
                    <asp:TextBox ID="txtDataDemissao" runat="server" MaxLength="10" onKeyUp="mascara_data(this)"></asp:TextBox>
                    <br />
                </asp:Panel>

                <asp:Panel runat="server" ID="panelAluno" Visible="false" class="panel panel-default" GroupingText="Dados do Aluno">
                    *Nome do Responsável<br />
                    <asp:TextBox ID="txtResponsavel" runat="server" Width="380px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvResponsavel" runat="server" ErrorMessage="Campo Obrigatório" ControlToValidate="txtResponsavel" ForeColor="Red" ViewStateMode="Disabled"></asp:RequiredFieldValidator><br />
                    <br />
                    Turma<br />
                    <asp:TextBox ID="txtTurma" runat="server" Width="380px"></asp:TextBox>
                    <br />
                    *Data de Matricula (dd/mm/aaaa)<br />
                    <asp:TextBox ID="txtDataMatric" runat="server" onKeyUp="mascara_data(this)" MaxLength="10"></asp:TextBox>
                    <br />
                    Data de Termino (dd/mm/aaaa)<br />
                    <asp:TextBox ID="txtDataTermino" runat="server" onKeyUp="mascara_data(this)" MaxLength="10"></asp:TextBox>
                    <br />
                    Valor da Mensalidade<br />
                    <asp:TextBox ID="txtValorMensalidade" runat="server"></asp:TextBox>
                    <br />
                    <br />
                    <asp:Panel runat="server" class="panel panel-default" GroupingText="Notas do Aluno">
                        Nota 1<br />
                        <asp:TextBox ID="txtNota1" runat="server"></asp:TextBox>
                        <br />
                        Nota 2<br />
                        <asp:TextBox ID="txtNota2" runat="server"></asp:TextBox>
                        <br />
                        Media Final<br />
                        <asp:TextBox ID="txtMediaFinal" runat="server"></asp:TextBox>
                        <br />
                    </asp:Panel>
                </asp:Panel>
                <asp:Label ID="Label1" runat="server" ForeColor="Red">* Campos Obrigatórios</asp:Label>
                <br />
                <br />
                <asp:Button runat="server" ID="btLimpar" Text="Limpar" OnClick="btLimpar_Click" />
                <asp:Button runat="server" ID="btSalvar" Text="Salvar" OnClick="btSalvar_Click" />
            </div>
        </div>
    </form>
</body>
</html>
